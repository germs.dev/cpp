---
title: C++ recipes
author: Dean Turpin
...

This isn't an exhaustive C++ reference -- see [DevDocs](https://devdocs.io/) for that -- but an exploration of regularly-used features or those that have piqued my interest.

_BTW, my other websites are [here](https://turpin.dev/)._

This code is mostly developed and formatted in [Godbolt](https://godbolt.org/z/3vvqnrMTP), which provides rapid feedback and a deeper insight into your code. To add a new example, I create a gtest to demonstrate it then simply add it to [`test.cxx`](https://gitlab.com/germs-dev/cpp/-/blob/main/test.cxx). On commit, the examples are compiled and run in a GitLab CI pipe and the C++ and test results are deployed as this HTML page.

See the [build pipeline](https://gitlab.com/germs-dev/cpp/-/blob/main/.gitlab-ci.yml) and [Doxygen documentation](html/test_8cxx.html) for this repo.

- __Unit test__ -- this is powerful and will almost certainly catch on
- __CI/CD__ -- get your releases automated from the outset
- __Premature optimization is the root of all evil__ -- write clean and correct code and _then_ measure performance: the conventional wisdom no longer applies if your data fit in the cache (see [Donald
Knuth](http://wiki.c2.com/?PrematureOptimization)); measure or it didn't happen
- __Minimise scope__ -- declare things just before you use them
- __clang-format__ -- auto format your code so you don't have to think about it
- __Make everything `const`__ –- it's much easier to reason about and parallelise if data are immutable
- __Use `auto`__ -- and test your asserts about what type it _should_ be with `static_assert` (you can even use concepts: `std::floating_point`)
- __Even better... make everything `constexpr`__ -- catch UB at compile time with `static_assert` (most of the Standard Library isn't `constexpr` yet but that doesn't mean you can't write your own versions)
- __Minimise 3rd-party library dependencies__ -- or at least minimise the scope of library use; it's really easy to let it creep into the whole codebase, and then the licensing changes and you're doomed. DOOMED!
- __TODO comments__ -- TODON'T do this! Count the instances of "todo" in your codebase to see how ineffective this is
- Minimise configuration options -- both at compile and run time; every additional option means more permutations that need testing, every large system I've worked on has required more time than "the heat death of the Sun" to test all combinations
- __Use the latest__ -- there's a curious thing in every company where you pin your tech stack on a certain version and then it's _a real effort_ to move foward; why not track the latest gcc and revert only when you want to release?

Also see [The Power of 10: Rules for Developing Safety-Critical Code](https://en.wikipedia.org/wiki/The_Power_of_10:_Rules_for_Developing_Safety-Critical_Code) for some related good practice.

___

