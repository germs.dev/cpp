#include <iostream>

class base {

public:
  base() {}

  virtual void id() { std::cout << "hi i am base\n"; }
};

class derived : public base {

public:
  derived() { id(); }

  void id() override { std::cout << "hi i am derived\n"; }
};

int main() {
  std::cout << "base:\n";
  base b;

  std::cout << "\nderived:\n";
  derived d;
}
